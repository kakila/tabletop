import logging

# disable logging when testing
logging.disable(logging.CRITICAL)
