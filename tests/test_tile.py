import doctest
import unittest
import tabletop.tile as m


# Doctest runner
def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite(m))
    return tests
