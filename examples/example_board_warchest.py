"""
War Chest board
================================
Build a War Chest board:

.. image:: https://cf.geekdo-images.com/t1p72-a1YGmNWKGFxvQHBg__imagepage/img/cEOPP49AKhOhCXRcnwRP6tm0uJM=/fit-in/900x600/filters:no_upscale():strip_icc()/pic4345750.jpg

"""

import numpy as np
import matplotlib.pyplot as plt

try:
    import tabletop
except ModuleNotFoundError:
    import sys, os

    sys.path.insert(0, os.path.abspath(".."))

from tabletop.boards import hexgrid
from tabletop.tile import Tile


# %%
# Set up the coordinate system
board = hexgrid.AxialHexGrid(size=(7, 7), orientation="flat")


# %%
# Set up the tiles
class CtrPoint(Tile):
    name = "control"
    colors = {"none": "green", "wolf": "grey", "raven": "black"}
    board_idx = None

    def __init__(self, *args, **kwargs):
        super().__init__(name=self.name, *args, **kwargs)
        self._owner = "none"
        self.index = None

    def locate(self, board_coord) -> int:
        return np.argmin(np.sum(np.abs(board_coord - self.coord), axis=1))

    @property
    def owner(self) -> str:
        return self._owner

    @owner.setter
    def owner(self, name: str):
        self._owner = name

    @property
    def color(self) -> str:
        return self.colors[self._owner]


ctr_coord = np.array(
    [
        [1, -1],
        [-1, 1],
        [-2, 0],
        [2, 0],
        [3, -2],
        [2, -3],  # raven
        [-1, -2],  # raven
        [-3, 2],
        [-2, 3],  # wolf
        [1, 2],  # wolf
    ]
)
wolf_idx = [8, 9]
raven_idx = [5, 6]


# %%
# Define encoding of the state of a WC board
def wc_split(wc: str | list[bool]) -> tuple[str, str] | tuple[list[bool], list[bool]]:
    return wc[:10], wc[10:]


def int_split(n: int) -> tuple[int, int]:
    return n >> 10, n & 1023


def int_merge(owned: int, owner: int) -> int:
    return (owned << 10) | owner


def int2wc(n: int, *, split: bool = False) -> str | tuple[str, str]:
    """Integer as a board state digits.


    Example
    -------
    >>> int2wc(121212)
    '00011101100101111100'
    >>> int2wc(121212, split=True)
    ('0001110110', '0101111100')
    """
    owned, owner = f"{(n >> 10):010b}", f"{(n & 1023):010b}"
    return owned + owner if not split else (owned, owner)


def wc2int(wc: str, *, split: bool = False) -> int | tuple[int, int]:
    """Integer as a board state digits

    A board state is encoded as a 20 bits binary number
    The 10 left most bits encode whether the control point is owned.
    The following 10 bits encode the ownership.

    For example the initial state for a 2 player games is
    '00000 11 0 11 00000 00 0 11' or '00000 11 0 11 00000 11 0 00'

    The digits follow a clockwise spiral order starting in the axial location [1, -1].
    """
    return int(wc, 2) if not split else tuple(map(lambda x: int(x, 2), wc_split(wc)))


def default_init(*, players: int = 2, split: bool = False) -> str:
    if players == 2:
        owned, owner = "0000011011", "0000000011"
    elif players == 4:
        raise NotImplementedError("4 players is not implemented")

    return owned + owner if not split else (owned, owner)


def wc2bool(
    wc: str, *, split: bool = False
) -> list[bool] | tuple[list[bool], list[bool]]:
    b = list(map(lambda x: x == "1", wc))
    return b if not split else wc_split(b)


def total_owned(code: int) -> int:
    return int_split(code)[0].bit_count()


def is_reachable(code: int | str, init: int | str = None) -> bool:
    if isinstance(code, int):
        code = int2wc(code)

    if init is None:
        init = default_init()
    if isinstance(init, str):
        init, _ = wc2int(init, split=True)
    owned, _ = wc2int(code, split=True)
    if (owned & init) != init:
        # the code's owned points do not contain the initial ones
        return False

    owned, owner = wc2bool(code, split=True)
    p1 = sum(o for a, o in zip(owned, owner) if a)
    p2 = sum(owned) - p1
    return (p1 <= 6) & (p2 <= 6)


def int_equiv(n: int) -> int:
    return (n ^ 1023) & (1023 << 10 | n >> 10)


def wc_equiv(wc: str) -> str:
    owned, owner = wc_split(wc)
    return owned + f"{int(owner, 2) ^ 1023:010b}"


def ctr2wc(ctr: list[CtrPoint]) -> str:
    owned, owner = int2wc(0, split=True)
    for p in ctr:
        owned[p.index] = "1" if p.owner != "none" else "0"
        owner[p.index] = "1" if p.owner == "wolf" else "0"
    return owned + owner


def wc2ctr(wc: str, brd: hexgrid.AxialHexGrid) -> list[CtrPoint]:
    """Create control points form wc board state code."""
    owned, owner = wc_split(wc)
    owned = tuple(map(lambda x: x == "1", owned))
    ctr = []
    for i, p in enumerate(ctr_coord):  # defines order
        t_ = CtrPoint(coord=p)
        # identify tile on board
        t_.index = t_.locate(brd.hexes)
        # set owner
        if owned[i]:
            t_.owner = "wolf" if owner[i] == "1" else "raven"
        ctr.append(t_)
    return ctr


def ctr_facecolors(ctr: list[CtrPoint], n: int) -> list[str]:
    """Array of face colors base don tiles ownership."""
    fc = ["none"] * n
    for t in ctr:
        fc[t.index] = t.color
    return fc


def int2ctr(n: int, brd: hexgrid.AxialHexGrid) -> list[CtrPoint]:
    """Create control points form wc board state number."""
    return wc2ctr(int2wc(n), brd)


# %%
start_2p_code = wc2int(default_init())
ctr_tiles = int2ctr(start_2p_code, board)

start_2p_code_twin = int_equiv(start_2p_code)
ctr_tiles_twin = int2ctr(start_2p_code_twin, board)

# %%
fig, axs = plt.subplots(ncols=2)
for ax, c_, n in zip(
    axs, (ctr_tiles, ctr_tiles_twin), (start_2p_code, start_2p_code_twin)
):
    board.plot(
        facecolors=ctr_facecolors(c_, board.hexes.shape[0]),
        ax=ax,
    )
    ax.set_title(f"{n}: {is_reachable(n)}")


# %%
# Reachable board state
init_owned, _ = wc2int(default_init(), split=True)
p = np.arange(1024, dtype=int)  # possibles values in a part of a code
a = np.unique([(x | init_owned) for x in p])  # valid owned values, must contain initial
bc = np.array([x.bit_count() for x in a])
idx_ = np.argsort(bc)
a, bc = a[idx_], bc[idx_]
feas = []
for o in p[:513]:  # only half, because it is symmetric, due to exchange raven <--> wolf
    f_ = [is_reachable(int_merge(int(a_), int(o))) for a_ in a]
    feas.append(f_)
feas = np.asarray(feas)

# %%
# Show reachable states
fig, ax = plt.subplots()
iax = ax.matshow(feas.T, cmap="Grays", interpolation="nearest")
# ax.set_ylabel("owned count")
# ax.set_yticklabels([bc[int(n)] if 0 <= n < len(bc) else "" for n in ax.get_yticks()])
ax.set_xlabel("owner code")

# Pick and advanced ones and plot
fig, axs = plt.subplots(ncols=2)
idx_a = 23
a_ = int(a[idx_a])
o = [253, 255]
for ax, o_ in zip(axs, o):
    code = int_merge(a_, o_)
    ctr = int2ctr(code, board)
    board.plot(facecolors=ctr_facecolors(ctr, board.hexes.shape[0]), ax=ax)
    ax.set_title(f"mat{[idx_a,o_]} = {code}: {is_reachable(code)}")

# %%
plt.show()
