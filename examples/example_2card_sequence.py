"""
Probability of a 2 card sequence
================================
Experimentally compute the probability of drawing two
items i na given order from a deck.
"""

import numpy as np

try:
    import tabletop
except ModuleNotFoundError:
    import sys, os

    sys.path.insert(0, os.path.abspath(".."))

import tabletop.deck as td
from tabletop.utils import issorted


# %%
#
def contains(hand, seq):
    return all(x in hand for x in seq)


def has_sequence(hand, seq):
    # assumes all elements in seq are in hand
    try:
        # has index method
        idx = [hand.index(x) for x in seq]
    except AttributeError:
        sorter = np.sort(hand)
        idx = sorter[np.searchsorted(hand, seq, sorter=sorter)]
    return issorted(idx)


# %%
# Counting hands with desired sequence
# ------------------------------------
deck = td.Deck(items=range(1, 13))
card_seq = [12, 6]  # results are the same for any sequence

total_hands, has_items, has_seq = 0, 0, 0

for hand in td.hands_from_deck(deck, hand_size=4):
    total_hands += 1
    if contains(hand, card_seq):
        has_items += 1
        if has_sequence(hand, card_seq):
            has_seq += 1

# %%
# Results
# -------
probs = dict(
    has_sequence_cards=has_items / total_hands, has_sequence=has_seq / total_hands
)
probs
