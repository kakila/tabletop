"""
Probability of all card sequence (counting)
===========================================
Compute the probability of drawing items in a given order
from a deck, by explicitly counting.
"""

import time
from itertools import product
import numpy as np
import pandas as pd

try:
    import tabletop
except ModuleNotFoundError:
    import sys, os

    sys.path.insert(0, os.path.abspath(".."))

import tabletop.deck as td
from tabletop.utils import issorted, flatten, isequal


# %%
#
def contains(hand, seq):
    return all(x in hand for x in seq)


def has_sequence(hand, seq):
    # assumes all elements in seq are in hand
    try:
        # has index method
        idx = [hand.index(x) for x in seq]
    except AttributeError:
        sorter = np.sort(hand)
        idx = sorter[np.searchsorted(hand, seq, sorter=sorter)]
    return issorted(idx)


# %%
# Counting hands with desired sequence
# ------------------------------------
cards = list(range(1, 12 + 1))
deck = td.Deck(items=cards)

hand_sizes = [4, 5, 6, 7]
M_hsz = max(hand_sizes)
n_hsz = len(hand_sizes)
seq_sizes = range(1, M_hsz + 1)
card_seqs = tuple((cards[:sz], sz) for sz in seq_sizes)

hand_seq = list(filter(lambda x: x[0] >= x[1], product(hand_sizes, seq_sizes)))
has_cards = [
    (
        [
            0,
        ]
        * len(seq_sizes)
    )[:hsz]
    for hsz in hand_sizes
]
has_seq = [
    (
        [
            0,
        ]
        * len(seq_sizes)
    )[:hsz]
    for hsz in hand_sizes
]

total_hands = [td.count_deck_hands(deck, hand_size=hsz) for hsz in hand_sizes]

prev_hand = []
t0 = time.time()
for nh, hand in enumerate(td.hands_from_deck(deck, hand_size=M_hsz)):
    for j, cs_s in enumerate(card_seqs):
        card_seq, sz = cs_s

        has_cards_, has_seq_ = False, False
        for i, hsz in enumerate(hand_sizes):

            if hsz < sz:
                continue

            hand_ = hand[:hsz]
            if isequal(hand_, prev_hand[:hsz]):
                # This hand was already analyzed
                continue

            if not has_cards_:
                # search for cards
                has_cards_ = contains(hand_, card_seq)
                if has_cards_:
                    # if this hand size has it, also all bigger ones
                    for i_ in range(i, n_hsz):
                        has_cards[i_][j] += 1

            if has_cards_ and not has_seq_:
                # search for sequence
                has_seq_ = has_sequence(hand_, card_seq)
                if has_seq_:
                    # if this hand size has it, also all bigger ones
                    for i_ in range(i, n_hsz):
                        has_seq[i_][j] += 1

            if has_cards_ and has_seq_:
                # found the sequence, no need to search in bigger hands
                break

    prev_hand = hand[:]

print(f"Elapsed {time.time() - t0:.2} s")

# %%
# Results
# -------
index = pd.MultiIndex.from_tuples(hand_seq, names=("hand_size", "sequence_length"))
counts = pd.DataFrame(
    data=dict(has_cards=flatten(has_cards), has_sequence=flatten(has_seq)),
    index=index,
    dtype=int,
)
counts["total_hands"] = flatten(
    [
        [
            t,
        ]
        * hsz
        for t, hsz in zip(total_hands, hand_sizes)
    ]
)
counts
# %%
probs = counts[["has_cards", "has_sequence"]].div(counts.total_hands, axis=0)
(probs * 100).round(2)
