Tutorial examples
=======================

.. 
    We use sphinx-gallery to generate examples.
    
    To write examples follow the instructions at https://sphinx-gallery.github.io/stable/syntax.html
