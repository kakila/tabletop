"""
Probability of all card sequence
================================
Compute the probability of drawing items in a given order
from a deck,
"""

import time
from itertools import product
import numpy as np
import pandas as pd

try:
    import tabletop
except ModuleNotFoundError:
    import sys, os

    sys.path.insert(0, os.path.abspath(".."))

import tabletop.deck as td
from tabletop.utils import issorted, flatten, isequal

# %%
# Counting hands with desired sequence
# ------------------------------------
cards = list(range(1, 12 + 1))
deck = td.Deck(items=cards)

hand_sizes = list(range(1, 10 + 1))
total_hands = [td.count_deck_hands(deck, hand_size=hsz) for hsz in hand_sizes]
hand_orderlen = list(filter(lambda x: x[0] >= x[1], product(hand_sizes, hand_sizes)))

has_cards = [
    td.hands_with_cards(o, items=deck.max_size, hand_size=hsz)
    for hsz, o in hand_orderlen
]
has_order = [
    td.hands_with_order(o, items=deck.max_size, hand_size=hsz)
    for hsz, o in hand_orderlen
]

# %%
# Results
# -------
index = pd.MultiIndex.from_tuples(hand_orderlen, names=("hand_size", "order_length"))
counts = pd.DataFrame(
    data=dict(has_cards=has_cards, has_order=has_order), index=index, dtype=int
)
counts["total_hands"] = flatten(
    [
        [
            t,
        ]
        * hsz
        for t, hsz in zip(total_hands, hand_sizes)
    ]
)
counts

# %%
probs = counts[["has_cards", "has_order"]].div(counts.total_hands, axis=0)
(probs * 100).round(2)
