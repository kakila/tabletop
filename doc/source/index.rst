.. tabletop documentation master file, created by
   sphinx-quickstart on Sun Aug 20 22:15:13 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tabletop's documentation!
====================================

.. toctree::
   :maxdepth: 3
   :caption: User manual:

   overview

   auto_examples/index.rst

.. toctree::
   :maxdepth: 2
   :caption: Reference manual:

   API



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
