=================
API documentation
=================

.. contents:: :local:

Main module -- :mod:`tabletop`
--------------------------------------------------------------------------------
.. automodule:: tabletop
   :members:
   :undoc-members:

Deck module -- :mod:`tabletop.deck`
--------------------------------------------------------------------------------
.. automodule:: tabletop.deck
   :members:
   :undoc-members:

Tile module -- :mod:`tabletop.tile`
--------------------------------------------------------------------------------
.. automodule:: tabletop.tile
   :members:
   :undoc-members:
