"""
    General util functions
"""

# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import numbers
import operator
from itertools import pairwise, starmap
from typing import Callable, Iterable, Sized, Type

import numpy as np


def issorted(iterable: Iterable, *, compare: Callable = operator.le):
    """Return True if iterable is sorted according to compare operator.

    Examples
    --------
    >>> issorted([1,2,3])
    True
    >>> issorted([3,2,1])
    False
    >>> issorted([1,2,3,2,5,8])
    False

    Notes
    -----
    Adapted from https://stackoverflow.com/a/17224104
    """
    return all(starmap(compare, pairwise(iterable)))


def flatten(iter: Iterable, *, as_type: Type = None):
    """Flatten a nested iterable.

    Examples
    --------
    >>> flatten([[1], [2,3], [4,5,6]])
    [1, 2, 3, 4, 5, 6]
    >>> flatten([[1], [2,3], [4,5,6]], as_type=tuple)
    (1, 2, 3, 4, 5, 6)
    >>> flatten([[1], [2,3], "abc"])
    [1, 2, 3, 'a', 'b', 'c']
    >>> flatten([[1], [2,3], ["abc"]])
    [1, 2, 3, 'abc']
    """
    if as_type is None:
        as_type = type(iter)
    return as_type(s for sl in iter for s in sl)


def isequal(iter1: Sized, iter2: Sized):
    """Check if all items of iterators are equal."""
    if len(iter1) == len(iter2):
        return all(starmap(operator.eq, pairwise((iter1, iter2))))
    else:
        return False


def int_to_base(n: int, b: int, *, length: int = None) -> list[int]:
    """Convert an integer to a list of digits in the given base.

    Examples
    --------
    >>> int_to_base(3**4 - 1, 3)
    [2, 2, 2, 2]
    >>> int_to_base(3**4 - 1, 3, length=5)
    [0, 2, 2, 2, 2]
    """
    if n == 0:
        digits = [0]
    else:
        digits = []
        while n:
            digits.append(int(n % b))
            n //= b
    if length is not None:
        pad = length - len(digits)
        if pad > 0:
            digits += [0] * pad
    return digits[::-1]


def base_to_int(digits: list[int], b: int) -> int:
    """Inverse of :func:int_to_base.

    Examples
    --------
    >>> base_to_int(int_to_base(44, 3, length=10), 3)
    44
    """
    return int("".join(map(str, digits)), b)
