"""
    Representation of a hexagonal grid
"""

from copy import deepcopy

# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Based on: https://www.redblobgames.com/grids/hexagons/
# Uses code from: https://github.com/RedFT/Hexy/tree/master

import numpy as np
from typing import LiteralString

from mpl_toolkits.mplot3d.proj3d import transform
from numpy.typing import ArrayLike
import matplotlib.pyplot as plt
from matplotlib.patches import RegularPolygon
from matplotlib.transforms import Affine2D
from matplotlib.collections import RegularPolyCollection

from . import _hexy as hex

type Hex = ArrayLike[int]
type HexOrientation = LiteralString["flat", "pointy"]


def plot_hexes(
    coord: np.ndarray[float],
    *,
    hex_radius: float,
    orientation: HexOrientation,
    facecolors: str | list[str] = "none",
    show_centers: bool = False,
    show_shape: bool = True,
    show_coords: bool = False,
    ax: plt.Axes = None,
):
    """Plot an array of hexes."""
    if ax is None:
        fig, ax = plt.subplots()

    if show_shape:
        if isinstance(facecolors, str):
            facecolors = [facecolors] * coord.shape[0]
        if len(facecolors) != coord.shape[0]:
            raise ValueError("Facecolors must match number of hexes")

        for xy, fc in zip(coord, facecolors):
            shp = RegularPolygon(
                xy,
                6,
                radius=hex_radius,
                orientation=0.0 if orientation == "pointy" else rad_ang,
                facecolor=fc,
                edgecolor="k",
            )
            ax.add_patch(shp)

    if show_centers:
        ax.scatter(*coord.T)

    if show_coords:
        for p in coord:
            ax.annotate(
                f"{cart_to_axial(p, radius=hex_radius)}",
                p,
                horizontalalignment="center",
                verticalalignment="center",
            )

    r_ = hex_radius * 1.05
    ax.set_xlim(coord[:, 0].min() - r_, coord[:, 0].max() + r_)
    ax.set_ylim(coord[:, 1].min() - r_, coord[:, 1].max() + r_)
    return ax


rad_ang = -np.pi / 6
flat_to_pointy_mat = np.array(
    [[np.cos(rad_ang), np.sin(rad_ang)], [-np.sin(rad_ang), np.cos(rad_ang)]]
)
pointy_to_flat_mat = flat_to_pointy_mat.T


# Matrix for converting axial coordinates to cartesian coordinates
axial_to_cart_mat = np.array([[np.sqrt(3), np.sqrt(3) / 2], [0, -3 / 2.0]])

# Matrix for converting cartesian to axial coordinates
cart_to_axial_mat = np.linalg.inv(axial_to_cart_mat)


def axial_to_cart(axial, radius):
    """
    Converts the location of a hex in axial form to cartesian coordinates.
    :param axial: The location of a hex in axial form. nx2
    :param radius: Radius of all hexagons.
    :return: `axial` in cartesian coordinates.
    """
    return radius * axial_to_cart_mat.dot(axial.T).T


def cart_to_axial(cart, radius):
    """
    Converts the location of a hex in cartesian form to axial coordinates.
    :param cart: The location of a hex in cartesian coordinates. nx2
    :param radius: Radius of all hexagons.
    :return: `cart` in axial coordinates.
    """
    return np.round(cart_to_axial_mat.dot(cart.T / radius).T)


def flat_to_pointy(hexes: np.ndarray[Hex] | Hex) -> np.ndarray[Hex] | Hex:
    if not isinstance(hexes, np.ndarray):
        hexes = np.array([np.asarray(Hex)])
    return hexes @ pointy_to_flat_mat


def pointy_to_flat(hexes: np.ndarray[Hex] | Hex) -> np.ndarray[Hex] | Hex:
    if not isinstance(hexes, np.ndarray):
        hexes = np.array([np.asarray(Hex)])
    return hexes @ flat_to_pointy_mat


class AxialHexGrid:
    """Hexgrid with axial coordinates."""

    def __init__(
        self,
        *,
        size: tuple[int, int],
        orientation: HexOrientation = "flat",
        origin: Hex = (0, 0, 0),
        hex_radius: int = 25,
    ):
        """

        Parameters
        ----------
        size
            Total amount of hexes in (q, r) directions.
        orientation
            Of the hexgrid: flat or pointy.
        origin
            The origin of the axial coordinates is in the center of the hexgrid.
            If this argument is given, it is translated to the given coordinates.
        """
        origin = np.asarray(origin)
        self.qsize, self.rsize = size
        self.hexes = hex.get_disk(origin, (min(*size) - 1) // 2)[:, :2]
        self.orientation = orientation
        self.hex_radius = hex_radius

    @property
    def coord(self):
        p = axial_to_cart(self.hexes, radius=self.hex_radius)
        if self.orientation == "flat":
            p = pointy_to_flat(p)
        return p

    def plot(self, **kwargs):
        ax = plot_hexes(
            self.coord,
            orientation=self.orientation,
            hex_radius=self.hex_radius,
            **kwargs,
        )
        ax.axis("off")
        return ax
