"""
    Representation of a deck of items
"""

# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
from typing import Iterable, Any, Sized
from itertools import chain, permutations
from functools import partial
from math import prod, factorial, comb

import numpy as np


class Deck:
    def __init__(
        self,
        *,
        size: int = None,
        items: Iterable = None,
        back_design=0,
        seed=None,
    ):
        """A deck of items.

        Examples
        ========
        >>> d = Deck(size=3)
        >>> print(d)
        >0 1 2<

        Using items
        >>> d = Deck(items="abc")
        >>> print(d)
        >a b c<
        >>> d = Deck(items="abc", size=5)
        >>> print(d)
        >a b c a b<

        Using items frequency
        >>> d = Deck(items={"a": 2, "b": 2, "c": 1})
        >>> print(d)
        >a a b b c<

        """
        self.back_design = back_design
        if isinstance(items, dict):
            # frequency dictionary
            self.items, self.ordering = build_from_items_freq(items)
        elif size is not None:
            # size was given
            if items is not None:
                # items are given
                self.items, self.ordering = build_from_items(items, size=size)
            else:
                # items will be integers
                self.items, self.ordering = build_from_size(size)
        elif items is not None:
            # items where given
            self.items, self.ordering = build_from_items(items)
        self.max_size = self.ordering.size
        self._top = 0  # pointer to top items
        self._bottom = self.max_size - 1  # pointer to bottom item

        # Random generator
        self.rnd_generator = np.random.default_rng(seed=seed)

    @property
    def active_ordering(self) -> np.ndarray[int]:
        return self.ordering[self.active_slice]

    @property
    def active_slice(self) -> slice:
        return slice(self._top, self._bottom + 1)

    @property
    def empty(self) -> bool:
        return self._top >= self.max_size

    @property
    def ordered_items(self) -> np.ndarray:
        return self.items[self.ordering]

    def __str__(self) -> str:
        items = self.ordered_items
        top_past = " ".join(map(str, items[: self._top]))
        future = " ".join(map(str, items[self._top : self._bottom + 1]))
        bottom_past = " ".join(map(str, items[self._bottom + 1 :]))
        return (
            top_past
            + (" " if top_past else "")
            + ">"
            + future
            + "<"
            + (" " if bottom_past else "")
            + bottom_past
        )

    def reset(self):
        self._top = 0
        self._bottom = self.max_size - 1

    def draw(self, n: int = 1, *, items: bool = True, from_bottom: bool = False):
        """Draw items.

        Examples
        ========
        >>> d = Deck(size=3)
        >>> print(d)
        >0 1 2<
        >>> d.draw()
        array([0])
        >>> print(d)
        0 >1 2<
        >>> d.draw(2)
        array([1, 2])
        >>> print(d)
        0 1 2 ><
        >>> d.empty
        True

        Drawing from the bottom
        >>> d.reset()
        >>> print(d)
        >0 1 2<
        >>> d.draw(2, from_bottom=True)
        array([2, 1])
        >>> print(d)
        >0< 1 2
        """
        retval = self.look(n, items=items, bottom=from_bottom)
        if from_bottom:
            self._bottom -= n
        else:
            self._top += n
        return retval

    def look(self, n: int = 1, *, items: bool = True, bottom: bool = False):
        """Look at items.

        Examples
        ========
        >>> d = Deck(size=3)
        >>> print(d)
        >0 1 2<
        >>> d.look()
        array([0])
        >>> print(d)
        >0 1 2<
        >>> d.draw()
        array([0])
        >>> print(d)
        0 >1 2<
        >>> d.look()
        array([1])
        >>> d.look(bottom=True)
        array([2])
        """

        idx = draw(n, order=self.active_ordering, reverse=bottom)
        if items:
            return self.items[idx]
        return idx

    def shuffle(self, *, ideal: bool = True):
        """Shuffle the deck.

        Examples
        ========
        >>> d = Deck(size=5, seed=list(map(ord,"abracadabra")))
        >>> print(d)
        >0 1 2 3 4<
        >>> _ = d.draw(); _ = d.draw(from_bottom=True)
        >>> print(d)
        0 >1 2 3< 4
        >>> d.shuffle()
        >>> print(d)
        0 >2 1 3< 4
        """
        if ideal:
            self.ordering[self.active_slice] = self.ideal_shuffle()
        else:
            raise NotImplementedError

    def ideal_shuffle(self) -> np.ndarray[int]:
        """Return an ideal shuffle of the active deck.

        Examples
        ========
        >>> d = Deck(size=5, seed=list(map(ord,"abracadabra")))
        >>> print(d)
        >0 1 2 3 4<
        >>> d.ideal_shuffle()
        array([2, 4, 0, 1, 3])
        >>> _ = d.draw(); _ = d.draw(from_bottom=True)
        >>> print(d)
        0 >1 2 3< 4
        >>> d.ideal_shuffle()
        array([2, 3, 1])
        """
        return self.rnd_generator.permutation(self.active_ordering)


def build_from_size(sz: int) -> tuple[np.ndarray, np.ndarray[int]]:
    """Build deck elements of a given size.

    Examples
    ========
    >>> build_from_size(3)
    (array([0, 1, 2]), array([0, 1, 2]))

    """
    items = np.arange(sz, dtype=int)
    ordering = np.arange(sz, dtype=int)
    return items, ordering


def build_from_items(
    items: Iterable, *, size: int = None
) -> tuple[np.ndarray, np.ndarray[int]]:
    """Build deck elements from iterable of items.

    Examples
    ========
    >>> build_from_items("abc")
    (array(['a', 'b', 'c'], dtype='<U1'), array([0, 1, 2]))
    >>> build_from_items("abc", size=5)
    (array(['a', 'b', 'c'], dtype='<U1'), array([0, 1, 2, 0, 1]))
    """
    if isinstance(items, str):
        items = list(items)
    items, ordering = np.unique(items, return_inverse=True)
    if size is not None:
        ordering = np.resize(ordering, size)
    return items, ordering


def build_from_items_freq(
    items_d: dict[Any, int]
) -> tuple[np.ndarray, np.ndarray[int]]:
    """Build deck elements from items frequency.

    Examples
    ========
    >>> build_from_items_freq({"a":2, "b": 2, "c": 1})
    (array(['a', 'b', 'c'], dtype='<U1'), array([0, 0, 1, 1, 2]))
    """
    items = np.asarray(list(items_d.keys()))
    freqs = [[i] * s for i, s in enumerate(items_d.values())]
    ordering = [item for freq in freqs for item in freq]
    ordering = np.asarray(ordering, dtype=int)
    return items, ordering


def draw(n: int, *, order: Iterable[int], reverse: bool = False) -> Iterable[int]:
    """Get the index of drawn items.

    Examples
    ========
    >>> draw(2, order=[1,0,2])
    [1, 0]
    >>> draw(2, order=[1,0,2], reverse=True)
    [2, 0]
    """
    if reverse:
        return order[::-1][:n]
    return order[:n]


def split_order(
    order: Iterable[int], *, chunks: int = 2, at: Iterable[int] = None
) -> tuple[Iterable[int], ...]:
    """Split an ordering array.

    Examples
    ========
    >>> split_order([1,2,3,4])
    ([1, 2], [3, 4])
    >>> split_order([1,2,3,4], chunks=3)
    ([1, 2], [3], [4])
    >>> split_order([1,2,3,4,5], at=[2,4])
    ([1, 2], [3, 4], [5])
    """
    if chunks < 2:
        raise ValueError("Only splits in at least 2 chunks.")
    if at is None:
        orders = np.array_split(order, chunks)
        if not isinstance(order, np.ndarray):
            orders = [o.tolist() for o in orders]
            orders = tuple(map(type(order), orders))
    else:
        # ignore chunks
        ats = zip(chain([0], at), chain(at, [len(order)]))
        orders = tuple(order[a:b] for a, b in ats)

    orders = tuple(o for o in orders if len(o) > 0)
    return orders


def split_deck(
    deck: Deck, *, chunks: int = 2, at: Iterable[int] = None
) -> tuple[Deck, ...]:
    """Split a deck.

    Examples
    ========
    >>> d = Deck(items="abcde")
    >>> print(d)
    >a b c d e<
    >>> d1, d2 = split_deck(d)
    >>> print(d1, d2, sep="\\n")
    >a b c<
    >d e<
    >>> decks = split_deck(d, chunks=3)
    >>> print(*decks, sep="\\n")
    >a b<
    >c d<
    >e<
    >>> decks = split_deck(d, at=[2,3])
    >>> print(*decks, sep="\\n")
    >a b<
    >c<
    >d e<
    """
    decks = tuple()
    for order in split_order(deck.ordering, chunks=chunks, at=at):
        decks += (Deck(items=deck.items[order]),)
    return decks


def merge_decks(*decks):
    """Merge decks into a single one.

    Examples
    ========
    >>> d = Deck(items="abcde")
    >>> print(d)
    >a b c d e<
    >>> d1, d2 = split_deck(d)
    >>> print(d1, d2, sep="\\n")
    >a b c<
    >d e<
    >>> d_ = merge_decks(d1, d2)
    >>> print(d_)
    >a b c d e<
    """
    return Deck(items=np.concatenate([d.ordered_items for d in decks]))


def random_hands_iter(
    ordered_items: int | Sized,
    *,
    hand_size: int = None,
    hands: int = np.inf,
    rnd_gen: np.random.Generator = None,
) -> np.ndarray:
    """Iterate randomly over hands generated by the given items.

    Examples
    --------
    >>> rng = np.random.default_rng(23732)
    >>> print(*list(random_hands_iter(5, hand_size=3, hands=5, rnd_gen=rng)), sep="\\n")
    [0 3 2]
    [0 3 4]
    [2 3 0]
    [2 3 4]
    [1 0 2]

    >>> from string import ascii_lowercase as letters
    >>> rng = np.random.default_rng(23732)
    >>> list(map("".join, random_hands_iter(list(letters), hand_size=5, hands=5, rnd_gen=rng)))
    ['aemgc', 'pvxbt', 'muxfz', 'rjmbd', 'pnbrc']
    """
    if rnd_gen is None:
        rnd_gen = np.random.default_rng()
    if hand_size is None:
        hand_size = len(ordered_items)

    hcount = 0
    choice = partial(rnd_gen.choice, replace=False, size=hand_size, shuffle=False)
    while hcount < hands:
        hcount += 1
        yield choice(ordered_items)


def random_hands_from_deck(
    deck: Deck, hand_size: int = None, hands: int = np.inf
) -> np.ndarray:
    """Iterate randomly over hands generated by the given deck.

    Examples
    --------
    >>> from string import ascii_lowercase as letters
    >>> deck = Deck(items=letters, seed=23732)
    >>> list(map("".join, random_hands_from_deck(deck, hand_size=3, hands=5)))
    ['aen', 'gcr', 'wyb', 'row', 'mfz']
    """
    return random_hands_iter(
        deck.ordered_items, hand_size=hand_size, hands=hands, rnd_gen=deck.rnd_generator
    )


def hands_iter(
    ordered_items: int | Sized, *, hand_size: int = None, hands: int = np.inf
) -> np.ndarray:
    """Iterate in order over hands generated by the given items.

    Examples
    --------
    >>> print(*list(hands_iter(3, hand_size=2)), sep="\\n")
    (0, 1)
    (0, 2)
    (1, 0)
    (1, 2)
    (2, 0)
    (2, 1)
    """
    if isinstance(ordered_items, int):
        ordered_items = range(ordered_items)
    perms = permutations(ordered_items, hand_size)
    n = 0
    while n < hands:
        try:
            yield next(perms)
        except StopIteration:
            break
        n += 1


def hands_from_deck(
    deck: Deck, hand_size: int = None, hands: int = np.inf
) -> np.ndarray:
    """Iterate in order over hands generated by the given deck.

    Examples
    --------
    >>> deck = Deck(size=3)
    >>> hands = hands_from_deck(deck, hand_size=2)
    >>> # to avoid nunpy types in string output
    >>> print(*list(map(lambda x: tuple(map(int, x)), hands)), sep="\\n")
    (0, 1)
    (0, 2)
    (1, 0)
    (1, 2)
    (2, 0)
    (2, 1)
    """
    return hands_iter(deck.ordered_items, hand_size=hand_size, hands=hands)


def count_hands(items: int | Sized, *, hand_size: int = None):
    """Count number of hands of given size.

    Examples
    --------
    >>> count_hands(12, hand_size=4) == len(list(hands_iter(12, hand_size=4)))
    True
    >>> count_hands("abc") == len(list(hands_iter("abc")))
    True

    """
    if isinstance(items, Sized):
        items = len(items)
    if hand_size is None:
        hand_size = items
    return prod(range(items, items - hand_size, -1))


def count_deck_hands(deck: Deck, *, hand_size: int = None):
    """Count number of hands of given size.

    Examples
    --------
    >>> d = Deck(size=12)
    >>> count_deck_hands(d, hand_size=4) == len(list(hands_from_deck(d, hand_size=4)))
    True
    >>> d = Deck(items="abc")
    >>> count_deck_hands(d) == len(list(hands_from_deck(d)))
    True

    """
    return count_hands(deck.max_size, hand_size=hand_size)


def hands_with_cards(cards: int, *, items: int, hand_size: int = None) -> float:
    """Number of hands with given cards."""
    if hand_size is None:
        hand_size = items
    if hand_size > items:
        ValueError("Cannot have more items in hand than items")
    if cards > hand_size:
        return 0.0

    # This perms(cards) * combs(hand_size, cards)
    used_slots = hand_size - cards
    cards_locations = prod(range(hand_size, used_slots, -1))

    free_slots = items - cards
    hands_per_card_location = prod(range(free_slots, free_slots - used_slots, -1))
    return hands_per_card_location * cards_locations


def cards_in_hand_probability(
    cards: int, *, items: int, hand_size: int = None
) -> float:
    """Probability of hanving cards in hand."""
    return hands_with_cards(cards, items=items, hand_size=hand_size) / count_hands(
        items, hand_size=hand_size
    )


def hands_with_order(order_len: int, *, items: int, hand_size: int = None) -> float:
    """Number of hands with card ordering of given length."""
    if hand_size is None:
        hand_size = items
    if hand_size > items:
        ValueError("Cannot have more items in hand than items")
    if order_len > hand_size:
        return 0.0

    used_slots = hand_size - order_len
    cards_locations = comb(hand_size, order_len)

    free_slots = items - order_len
    hands_per_card_location = prod(range(free_slots, free_slots - used_slots, -1))
    return hands_per_card_location * cards_locations


def order_in_hand_probability(
    cards: int, *, items: int, hand_size: int = None
) -> float:
    """Probability of hanving ordered cards in hand."""
    return hands_with_order(cards, items=items, hand_size=hand_size) / count_hands(
        items, hand_size=hand_size
    )
