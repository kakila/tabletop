"""
    Representation of a tile
"""

# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
from typing import Iterable, Any, Sized
from itertools import count
from textwrap import dedent, indent

import numpy as np


class Tile:
    _count = count(0)

    def __init__(
        self,
        *,
        coord: Iterable = None,
        contents: Iterable = None,
        name: str = None,
    ):
        """A tile.

        Examples
        --------
        >>> tiles = [Tile(coord=[0,0], name="black", contents=dict(color="black")),
        ... Tile(coord=[1,0], name="white", contents=dict(color="white"))]
        >>> _ = [print(x) for x in tiles]  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
        Tile 'black'.
        At [0 0].
        Containing {'color': 'black'}.
        Tile 'white'.
        At [1 0].
        Containing {'color': 'white'}.
        """
        self.id = next(Tile._count)

        self.name = name
        self._coord = np.asarray([]) if coord is None else np.asarray(coord)
        self.contents = [] if contents is None else contents

    @property
    def coord(self) -> np.ndarray:
        return self._coord

    @coord.setter
    def coord(self, value: Iterable):
        self._coord = np.asarray(value)

    @property
    def isplaced(self) -> bool:
        """Whether the tile has a location.

        Examples
        --------
        >>> x = Tile()
        >>> x.isplaced
        False
        >>> x.coord = (0,0)
        >>> x.isplaced
        True
        """
        return self._coord.size > 0

    def at(self, value: Iterable):
        """Set the coordinates of a tile.

        Similar to 'x.coord = value' but checking that the value
        conserves the shape of 'x.coord'

        Examples
        --------
        >>> x = Tile(coord=[0,0])
        >>> print(x)  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
        Tile ...
        At [0 0].
        Containing nothing.
        >>> x.at([0,1])
        >>> print(x)  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
        Tile ...
        At [0 1].
        Containing nothing.

        Seeting coordinates directly can chage their shape
        >>> x.coord = [[0],[0]]
        >>> print(x)  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
        Tile ...
        At [[0]
            [0]].
        Containing nothing.
        """
        if not self.isplaced:
            self.coord = value
        else:
            self._coord = np.asarray(value).reshape(self._coord.shape)

    def displace(self, value: Iterable):
        """Displace the coordianates of a tile.

        Examples
        --------
        >>> x = Tile(coord=[0,0])
        >>> print(x)  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
        Tile ...
        At [0 0].
        Containing nothing.
        >>> x.displace([0,1])
        >>> print(x)  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
        Tile ...
        At [0 1].
        Containing nothing.
        """
        if not self.isplaced:
            raise ValueError("The tile is nowhere, can't displace")
        self._coord += np.asarray(value).reshape(self._coord.shape)

    def transform(self, T: np.ndarray):
        """Apply an arbitrary linear transformation to the coordinate of the tile."""
        if not self.isplaced:
            raise ValueError("The tile is nowhere, can't transform")
        self._coord = T @ self._coord

    def __str__(self):
        _name = f"'{self.id if self.name is None else self.name}'"
        _loc = f"{self._coord if self.isplaced else 'nowhere'}"
        _loc_n = len(str(_loc).splitlines())
        if _loc_n > 1:
            _loc = "\n".join(
                [
                    l if n == 0 else " " * (8 + 3) + l
                    for n, l in enumerate(_loc.splitlines())
                ]
            )
        _cont = f"{'nothing' if not self.contents else self.contents}"
        txt = dedent(
            f"""\
        Tile {_name}.
        At {_loc}.
        Containing {_cont}."""
        )
        return txt
