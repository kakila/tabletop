import importlib
import warnings
import tomllib
from importlib.metadata import metadata
from pathlib import Path
from datetime import datetime as dt

try:
    # Import this package
    # this adds it to the path so metadata works
    module_ = importlib.import_module(__name__)

    # Cast Message to Dictionary
    meta_ = dict(metadata(__name__).items())
    # Use lowercase keys
    meta_ = {k.lower(): v for k, v in meta_.items()}
    # get author from author-email
    author_ = meta_["author-email"].split("<")[0].strip()
    del module_

except importlib.metadata.PackageNotFoundError:
    warnings.warn(
        f"The package '{__name__}' was not found by importlib! "
        "Reading metadata from file"
    )

    __metadata_name__ = "pyproject.toml"
    __metadata_folder__ = Path(__file__).parent.resolve() / ".."
    __metadata_filepath__ = __metadata_folder__ / __metadata_name__
    if not __metadata_filepath__.exists():
        raise ImportError("No package metadata file found!")

    with __metadata_filepath__.open(mode="rb") as f:
        meta_ = tomllib.load(f)["project"]
    # get decription from readme
    meta_["description"] = (__metadata_folder__ / meta_["readme"]["file"]).read_text()
    # extract license text
    meta_["license"] = meta_["license"]["text"]

    author_ = meta_["authors"][0]["name"]

# get description from brief in README
# title - markup - brief
meta_["description"] = [x for x in meta_["description"].splitlines() if x][2]
meta_["copyright"] = f"Copyright (C) {dt.now().year} " + author_

# Add are fields in metadata to module's privates
__version__ = meta_["version"]
__author__ = author_
__license__ = meta_["license"]
__copyright__ = meta_["copyright"]
__description__ = meta_["description"]

del meta_, author_


# Make simple function to check package installation
def describe():
    """Print package information."""
    from rich.console import Console

    console = Console()
    print = console.print

    print(f"[b]Package[/b]: {__name__} [bold cyan]{__version__}[/bold cyan]")
    print(f"[b]Description[/b]: {__description__}")
    print(f"[b]Authors[/b]: {__author__}")
    console.rule("[bold red] License")
    print(__copyright__ + "\n", style="bold")
    print(__license__)
